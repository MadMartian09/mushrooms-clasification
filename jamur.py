# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 07:39:35 2020

@author: ASUS
"""

#import libary
import pandas as pd
from sklearn.preprocessing import LabelEncoder
import numpy as np 


##Baca data jamur
# data = pd.read_csv('D:\KULIAH\Pengenalan Pola\mushrooms.csv')
##Pilah kelas jamur
# p = data.loc[(data['class'])=='p']
# e = data.loc[(data['class'])=='e']
##Gabung kelas jamur
# result = [(p.sample(n=25)), (e.sample(n=25))]
# result = pd.concat(result)
# print(result)
#export csv
# result.to_csv (r'D:\KULIAH\Pengenalan Pola\jamur50.csv', index = False, header=True)

#baca data hasil pengambilan data
data = pd.read_excel('D:\KULIAH\Pengenalan Pola\jamur50.xlsx')
#kuantisasi data
lb = LabelEncoder()
data1 = data.apply(lb.fit_transform)
##export hasil kuantisasi
#data1.to_csv(r'D:\KULIAH\Pengenalan Pola\jamurK.csv', index = False, header=True)

#lacak hasil kuantisasi
for i in (data1.columns) :
    print(i)
    lb.fit(data[i])
    tes =lb.transform(data[i])
    print(np.unique(tes))
    print(list(lb.inverse_transform(np.unique(tes))))
    





