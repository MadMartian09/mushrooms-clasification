# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 09:33:10 2020

@author: ASUS
"""


import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
# from sklearn.model_selection import train_test_split
# from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
# from sklearn.metrics import classification_report
# from sklearn.model_selection import cross_val_score

dataset1 = pd.read_excel("D:\KULIAH\Pengenalan Pola\ProjekJamur.xlsx", 
                             sheet_name='Dataset 1')
dataset2 = pd.read_excel("D:\KULIAH\Pengenalan Pola\ProjekJamur.xlsx", 
                             sheet_name='Dataset 2')
dataset3 = pd.read_excel("D:\KULIAH\Pengenalan Pola\ProjekJamur.xlsx", 
                             sheet_name='Dataset 3')


x1_train = dataset1.iloc[0:32]
x1_test = dataset1.iloc[33:51]
x2_train = dataset2.iloc[0:32]
x2_test = dataset2.iloc[33:51]
x3_train = dataset3.iloc[0:32]
x3_test = dataset3.iloc[33:51]
xtrain = [x1_train, x2_train, x3_train]
xtest = [x1_test, x2_test, x3_test]

y1_train = dataset1.iloc[0:32]["class"]
y1_test = dataset1.iloc[33:51]["class"]
y2_train = dataset2.iloc[0:32]["class"]
y2_test = dataset2.iloc[33:51]["class"]
y3_train = dataset3.iloc[0:32]["CLASS"]
y3_test = dataset3.iloc[33:51]["CLASS"]
ytrain = [y1_train, y2_train, y3_train]
ytest = [y1_test, y2_test, y3_test]

knn = KNeighborsClassifier(n_neighbors=5, metric='cityblock')
knn.fit(x1_train, y1_train)
y_pred = knn.predict(x1_test)
print(y_pred)



acc = 0

for i in range (len(xtrain)):
    print("=========== DATASET", i+1, "=========", "5-NN")
    knn = KNeighborsClassifier(n_neighbors=5, metric='cityblock')
    knn.fit(xtrain[i], ytrain[i])
    y_pred = knn.predict(xtest[i])
    print("Hasil Prediksi: ",y_pred)
    print("Confusion matrix")
    cmat=confusion_matrix(ytest[i], y_pred)
    print(cmat)
    print("Akurasi : ",knn.score(xtest[i], ytest[i]))
    acc = acc+knn.score(xtest[i], ytest[i])
    
    #print (classification_report(ytest[i], y_pred))
print("================================")
print("Akurasi Rata Rata : \n", acc/3)
